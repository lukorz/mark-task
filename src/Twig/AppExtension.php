<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * AppExtension
 */
class AppExtension extends AbstractExtension
{
    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('base64Encoded', [$this, 'base64EncodedFilter']),
        ];
    }

    /**
     * @param stream $imageFile
     *
     * @return string
     */
    public function base64EncodedFilter($imageFile): string
    {
        return base64_encode(stream_get_contents($imageFile));
    }
}
