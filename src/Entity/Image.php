<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Image
 *
 * @ORM\Entity()
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="blob")
     * @Assert\Image()
     */
    private $imageFile;

    /**
     * @ORM\Column()
     */
    private $mimeType;

    /**
     * @ORM\Column()
     */
    private $imageName;

    /**
     * @ORM\Column()
     */
    private $userName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set id
     *
     * @param int $id
     *
     * @return Image
     */
    public function setId(int $id): Image
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set image file
     *
     * @param string $imageFile
     *
     * @return Image
     */
    public function setImageFile(string $imageFile): Image
    {
        $this->imageFile = $imageFile;

        return $this;
    }

    /**
     * Get image file
     *
     * @return stream
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set mime type
     *
     * @param string $mimeType
     *
     * @return Image
     */
    public function setMimeType(string $mimeType): Image
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * Get mime type
     *
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    /**
     * Set image name
     *
     * @param string $imageName
     *
     * @return Image
     */
    public function setImageName(string $imageName): Image
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get image name
     *
     * @return string
     */
    public function getImageName(): string
    {
        return $this->imageName;
    }

    /**
     * Set user name
     *
     * @param string $userName
     *
     * @return Image
     */
    public function setUserName(string $userName): Image
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get user name
     *
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * Set created at
     *
     * @param \DateTime $createdAt
     *
     * @return Image
     */
    public function setCreatedAt(\DateTime $createdAt): Image
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get created at
     *
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }
}
