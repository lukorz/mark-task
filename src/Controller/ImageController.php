<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Form\ImageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use App\Entity\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * ImageController
 *
 * @Route("/image")
 */
class ImageController extends Controller
{
    /**
     * @Route("/upload", name="image_upload")
     *
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function imageUploadAction(Request $request)
    {
        $form = $this->createForm(ImageType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /* @var $image Image */
            $image = $form->getData();
            /* @var $imageFile UploadedFile */
            $imageFile = $form->get('imageFile')->getData();
            $imageName = $imageFile->getClientOriginalName();
            $stream = fopen($imageFile->getRealPath(),'rb');
            $image->setImageFile(stream_get_contents($stream))
                ->setMimeType($imageFile->getClientMimeType())
                ->setImageName($imageName)
                ->setUserName($this->getUser()->getUsername());
            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush();
            $this->addFlash('success', 'Uploaded image: '.$imageName);

            return $this->redirectToRoute('image_list');
        }

        return $this->render('image/upload.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/list", name="image_list")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function imageListAction(Request $request)
    {
        $pageRange = abs($request->query->getInt('page_range', $this->getParameter('page_range_default')));
        $images = $this->getDoctrine()
            ->getManager()
            ->getRepository('App:Image')
            ->findAll();
        /* @var $paginator \Knp\Component\Pager\Paginator */
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $images,
            $request->query->getInt('page', 1),
            $pageRange
        );

        return $this->render('image/list.html.twig', [
            'pagination' => $pagination,
        ]);
    }
}
