# README #

### Steps to run application ###

* Requirements: Composer, PHP 7.2+, PHP SQLite driver
* Run "composer:install"
* Run "bin/console doctrine:schema:create"
* Run "bin/console server:start"
* Users credentials: John johnpass, Tom tompass, Alice alicepass
